//
//  AppDelegate.h
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/19/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end

//
//  ImageManager.m
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/20/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import "ImageManager.h"
#import "AFNetworking.h"

@implementation ImageManager

+(void) fetchOrSetImageView:(UIImageView *)view post:(Post *)post
{
    NSString *urlString = post.thumbnailURLString;
    if (urlString.length == 0)
        return;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    __weak UIImageView *imageView = view;

    
    [imageView setImageWithURLRequest:request placeholderImage:[ImageManager placeholderImage] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        imageView.image = image;
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        NSLog(@"Error loading image - %@", [error localizedDescription]);
    }];
    
}

+ (UIImage *)placeholderImage
{
    // this could be whatever, preferably something that makes
    // sense given the context, but I'm using this for the sake of example.
    return [UIImage imageNamed:@"default_pic"];
}

@end

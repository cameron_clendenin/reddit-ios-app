//
//  Post.m
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/19/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import "Post.h"
#import "SBJson.h"

@implementation Post

@synthesize thumbnailURLString =_thumbnailURLString;
@synthesize thumbnailImage =_thumbnailImage;
@synthesize author = _author;
@synthesize title = _title;

static CGFloat const MIN_HEIGHT_CELL = 76.0f;

-(id) initWithJSON:(id)json
{
    self = [super init];
    if (self) {
        id data = [json objectForKey:@"data"];
        
        self.thumbnailURLString = [data objectForKey:@"thumbnail"];
        self.author = [data objectForKey:@"author"];
        self.title = [data objectForKey:@"title"];
        
        [self setCellHeightWithFont:[Post fontForPostTitle]];
    }
    return self;
}


-(void) setCellHeightWithFont:(UIFont *)cellFont
{
    CGFloat bodyHeight = [_title sizeWithFont:cellFont constrainedToSize:CGSizeMake(210, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping].height;
    
    _cellHeight = MAX(MIN_HEIGHT_CELL, bodyHeight + 38.0f);
}

+ (UIFont *)fontForPostTitle
{
    return [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
}

@end

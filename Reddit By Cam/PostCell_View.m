//
//  PostCell_View.m
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/20/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import "PostCell_View.h"

@implementation PostCell_View

@synthesize cell = _cell;

- (id)initWithFrame:(CGRect)frame cell:(PostCell *)cell_
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setFrame:frame];
        self.cell = (PostCell *)cell_;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    if (self.cell.selected || self.cell.highlighted) {

    } else {
        
    }
    
    // author in blue custom font
    [[UIColor colorWithRed:.12 green:.65 blue:.99 alpha:1.0] setFill];
    CGContextSetShadow(UIGraphicsGetCurrentContext(), CGSizeMake(0, .5), .5);
    NSString *author = _cell.author;
    CGRect authorRect = CGRectMake(92, 4, 200, 22);
    UIFont *authorFont = [UIFont fontWithName:@"BebasNeue" size:22.0f];
    [author drawInRect:authorRect
              withFont:authorFont
         lineBreakMode:NSLineBreakByClipping
             alignment:NSTextAlignmentLeft];    
}


@end

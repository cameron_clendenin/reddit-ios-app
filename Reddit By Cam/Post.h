//
//  Post.h
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/19/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Post : NSObject


+ (UIFont *)fontForPostTitle;

// designated initializer
-(id) initWithJSON:(id)json;

// calculates and stores the height that the cell should be when displayed.
-(void) setCellHeightWithFont:(UIFont *)cellFont;

@property (nonatomic, strong) NSString *thumbnailURLString;
@property (nonatomic, strong) UIImage *thumbnailImage;
@property (nonatomic, strong) NSString *author;
@property (nonatomic, strong) NSString *title;

// store calculated cell height for quick future use
@property (nonatomic, assign) CGFloat cellHeight;


@end

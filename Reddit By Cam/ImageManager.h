//
//  ImageManager.h
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/20/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post.h"

@interface ImageManager : NSObject

// retrieves and sets the appropriate thumbnail image for a given post
+(void) fetchOrSetImageView:(UIImageView *)view post:(Post *)post;

@end

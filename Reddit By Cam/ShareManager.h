//
//  ShareManager.h
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/22/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//
// Share Manager encapsulates SMS and Email specific behavior.

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import "Post.h"

@protocol ShareManagerDelegate <NSObject>

- (void)sharingFinished; // called after message is sent or cancelled.

@end


@interface ShareManager : NSObject <MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

- (id) initWithDelegate:(id <ShareManagerDelegate>)delegate;

- (void) sharePostViaEmail:(Post *)post;

- (void) sharePostViaSMS:(Post *)post;


// delegate to be notified when sharing is finished
@property (nonatomic, weak) id <ShareManagerDelegate> delegate;

@end

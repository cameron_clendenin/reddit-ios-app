//
//  ViewController.m
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/19/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import "ViewController.h"
#import "PostCell.h"
#import "ImageManager.h"

static NSString *const SEARCHBAR_PLACEHOLDER = @"Search Reddit...";
static NSString *const SEARCHBAR_DEFAULT_SEARCH_TERM = @"funny";

static int REFRESH_TAG = 22;

@interface ViewController ()

@property (nonatomic, weak) Post *sharedPost;
@end

@implementation ViewController

@synthesize posts = _posts;

- (void)viewDidLoad
{
    [super viewDidLoad];
       
    _posts = [NSMutableArray array];
    
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.jpg"]];
    backgroundView.frame = self.view.bounds;
    tableView.backgroundView = backgroundView;
    tableView.delegate = self;
    tableView.dataSource = self;
    
    searchBar.placeholder = SEARCHBAR_PLACEHOLDER;
    searchBar.text = SEARCHBAR_DEFAULT_SEARCH_TERM;
    searchBar.delegate = self;

    if (NSClassFromString(@"UIRefreshControl")) {
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
        refreshControl.tag = REFRESH_TAG;
        refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Updating..."];
        [refreshControl addTarget:self action:@selector(fetchPosts) forControlEvents:UIControlEventValueChanged];
        [tableView addSubview:refreshControl];
    }
    
    // start search with default search term
    [self fetchPosts];
}

# pragma mark -
# pragma mark SearchBar Delegate

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar_
{
    [searchBar_ setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar_
{
    [searchBar_ resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar_
{
    [searchBar_ setShowsCancelButton:NO animated:YES];
    [searchBar_ resignFirstResponder];
    [self fetchPosts];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar_
{
    [searchBar_ setShowsCancelButton:NO animated:YES];
    [searchBar_ resignFirstResponder];
}

#pragma mark -
#pragma mark RequestManager Delegate

- (void)requestFinishedWithResults:(NSArray *)results
{
    if (results.count == 0) {
        // Decide on best way to handle zero results found.
        // Possibly leave existing list in place and show some
        // custom alert that instructs user to enter a new query
    }
    
    
    if (_posts == nil)
        _posts = [results copy];
    else {
        [_posts removeAllObjects];
        [_posts addObjectsFromArray:results];
    }
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    
    if (NSClassFromString(@"UIRefreshControl")) {
        UIRefreshControl *refreshControl = (UIRefreshControl*)[tableView viewWithTag:REFRESH_TAG];
        [refreshControl endRefreshing];
    }
    
    //
    [tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}

- (void)requestFailedWithError:(NSError *)error
{
    
}

# pragma mark -
# pragma mark TableView Delegate

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [(Post *)[_posts objectAtIndex:indexPath.row] cellHeight];
}

# pragma mark -
# pragma mark TableView Data Source

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _posts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView_ cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    PostCell *cell = [tableView_ dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = (PostCell *)[[PostCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    Post *selectedPost = [_posts objectAtIndex:indexPath.row];
    
    cell.author = selectedPost.author;
    cell.title = selectedPost.title;
    [ImageManager fetchOrSetImageView:cell.thumbnailImageView post:selectedPost];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView_ didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView_ deselectRowAtIndexPath:indexPath animated:NO];
    
    Post *selctedPost = [_posts objectAtIndex:indexPath.row];
    [self showShareViewForPost:selctedPost];
}

#pragma mark -
#pragma mark Helpers

- (void) fetchPosts
{
    [RequestManager fetchResultsForSearchQuery:searchBar.text withDelegate:self];
}

#pragma mark -
#pragma mark ShareView methods

- (void) showShareViewForPost:(Post *)post_
{
    self.sharedPost = post_;
    
    if (dimmingView == nil) {
        dimmingView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dimmer"]];
        dimmingView.frame = [UIScreen mainScreen].bounds;
        dimmingView.alpha = 0; // we'll increase this later.
        dimmingView.userInteractionEnabled = NO;
    }
    [self.view addSubview:dimmingView];
    
    // set share sheet position offscreen
    shareView.frame = CGRectMake(320, 480, 320, 480);
    [self.view addSubview:shareView];
    
    [UIView animateWithDuration:.25 animations:^{
        dimmingView.alpha = 1.0f;
        shareView.frame = CGRectMake(0, 0, 320, 480);
    } completion:^(BOOL finished) {
    
    }];
}

- (IBAction)dismissShareView:(id)sender
{
    [UIView animateWithDuration:.25 animations:^{
        dimmingView.alpha = 0;
    shareView.frame = CGRectMake(320, 480, 320, 480);
    } completion:^(BOOL finished) {
        [dimmingView removeFromSuperview];
        [shareView removeFromSuperview];
    }];
}

- (IBAction)shareViaSMSPressed:(id)sender
{
    if (shareManager == nil)
        shareManager = [[ShareManager alloc] initWithDelegate:self];
    
    [shareManager sharePostViaSMS:_sharedPost];
}

- (IBAction)shareViaEmailPressed:(id)sender
{
    if (shareManager == nil)
        shareManager = [[ShareManager alloc] initWithDelegate:self];
    
    [shareManager sharePostViaEmail:_sharedPost];
}

#pragma mark -
#pragma mark Share manager delegate

- (void)sharingFinished
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self dismissShareView:nil];
    }];
}

#pragma mark -
#pragma mark Other

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    dimmingView = nil;
}

@end

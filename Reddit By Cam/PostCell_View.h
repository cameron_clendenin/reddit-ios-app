//
//  PostCell_View.h
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/20/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostCell.h"

@interface PostCell_View : UIView

@property PostCell *cell;

- (id)initWithFrame:(CGRect)frame cell:(PostCell *)cell_;

@end

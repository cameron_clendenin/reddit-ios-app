//
//  ShareManager.m
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/22/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import "ShareManager.h"

@implementation ShareManager

- (id) initWithDelegate:(id <ShareManagerDelegate>)delegate_
{
    self = [super init];
    if (self) {
        self.delegate = delegate_;
    }
    return self;
}

- (void)sharePostViaEmail:(Post *)post
{
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	if (controller!=nil)
	{
		controller.mailComposeDelegate = self;
		
		[controller setSubject:@"Check this out from reddit"];
		NSString *emailBody = [NSString stringWithFormat:@"%@ - %@", post.author, post.title];
		[controller setMessageBody:emailBody isHTML:YES];
		[(UIViewController *)_delegate presentModalViewController:controller animated:YES];
	}
    
}

- (void)sharePostViaSMS:(Post *)post
{
    if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *smsController = [[MFMessageComposeViewController alloc] init];
        smsController.messageComposeDelegate = self;
        smsController.body = [NSString stringWithFormat:@"%@ - %@", post.author, post.title];
        [(UIViewController *)_delegate presentViewController:smsController animated:YES completion:^{
        }];
    } else {
        // handle non supported devices here...
    }
}


#pragma mark -
#pragma mark MFMail delegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error
{
	switch (result) {
		case MFMailComposeResultSent:
            [_delegate sharingFinished];
			break;
        case MFMailComposeResultCancelled:
            [_delegate sharingFinished];
			break;
		case MFMailComposeResultSaved:
            [_delegate sharingFinished];
			break;
        case MFMailComposeResultFailed:
		{
			UIAlertView *alert = [[UIAlertView alloc]
								  initWithTitle:@"We're having trouble sending the email."
								  message:@"Please check your internet connection and try again."
								  delegate:nil
								  cancelButtonTitle:@"Ok"
								  otherButtonTitles:nil];
			[alert show];
			break;
		}            
		default:
            [_delegate sharingFinished];
			break;
	}
}

#pragma mark -
#pragma mark MFMessage delegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [_delegate sharingFinished];
}


@end

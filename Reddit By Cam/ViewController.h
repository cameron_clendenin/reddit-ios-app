//
//  ViewController.h
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/19/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestManager.h"
#import "ShareManager.h"

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,
                            UISearchBarDelegate, RequestManagerDelegate, ShareManagerDelegate> {
    
    IBOutlet UISearchBar *searchBar;
    IBOutlet UITableView *tableView;
    
    // Dims tableView when searchBar is first responder.
    UIView *dimmingView;
                                
    IBOutlet UIView *shareView;
                               
    // handles sharing via sms and email
    ShareManager *shareManager;
}


// called when user taps (x) on share dialog
- (IBAction)dismissShareView:(id)sender;

// open new email dialog
- (IBAction)shareViaEmailPressed:(id)sender;

// open new SMS dialog
- (IBAction)shareViaSMSPressed:(id)sender;


@property (nonatomic, strong) NSMutableArray *posts;

@end

//
//  RequestManager.m
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/20/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import "RequestManager.h"
#import "JSONKit.h"
#import "Post.h"

@implementation RequestManager

+ (RequestManager *) instance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

+ (void)fetchResultsForSearchQuery:(NSString *)query withDelegate:(id <RequestManagerDelegate>)delegate_
{
    [[RequestManager instance] setDelegate:delegate_];
    
    // reddit categories are only one word, so we clip whitespace
    NSString *queryString = [NSString stringWithFormat:@"http://www.reddit.com/r/%@/.json", [query stringByTrimmingCharactersInSet:
                                                                                             [NSCharacterSet whitespaceCharacterSet]]];
    NSURL *url = [NSURL URLWithString:queryString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
       NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                               id jsonBlob = [str objectFromJSONString];
                               [RequestManager parseAndSortResults:jsonBlob];
    }];
}

+ (void) parseAndSortResults:(id)results
{
    NSMutableArray *posts = [NSMutableArray array];
    id jsonList = [[results objectForKey:@"data"] objectForKey:@"children"];
    for (id p in jsonList) {
        Post *newPost = [[Post alloc] initWithJSON:p];
        [posts addObject:newPost];
    }
    
    RequestManager *manager = [RequestManager instance];
    [manager.delegate requestFinishedWithResults:posts];
}

@end

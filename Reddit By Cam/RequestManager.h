//
//  RequestManager.h
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/20/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RequestManagerDelegate <NSObject>

- (void)requestFinishedWithResults:(NSArray *)results; // return array of post objects

@optional
- (void)requestFailedWithError:(NSError *)error;

@end

@interface RequestManager : NSObject

+ (RequestManager *) instance;

/* 
    Queries reddit api with provided search term.
    Delegate is notfied when search is complete
 */
+ (void)fetchResultsForSearchQuery:(NSString *)query withDelegate:(id <RequestManagerDelegate>)delegate_;

@property (nonatomic, assign) id <RequestManagerDelegate> delegate;

@end

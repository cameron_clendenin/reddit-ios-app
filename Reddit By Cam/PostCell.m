//
//  PostCell.m
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/20/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import "PostCell.h"
#import "PostCell_View.h"
#import "Post.h"
#import <QuartzCore/QuartzCore.h>

@implementation PostCell

@synthesize customView = _customView;
@synthesize thumbnailImageView = _thumbnailImageView;
@synthesize author = _author;
@synthesize title = _title;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // do drawing for author / title in this view
        _customView = [[PostCell_View alloc]
                           initWithFrame:self.bounds cell:self];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:_customView];
        
        // make permanent shadow behind imageview
        UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(20, 8, 60, 60)];
        shadowView.layer.shadowColor = UIColor.blackColor.CGColor;
        shadowView.layer.shadowOpacity = .7f;
        shadowView.layer.shadowOffset = CGSizeMake(1, 1);
        shadowView.layer.shadowRadius = 4.0f;
        CGPathRef path = [UIBezierPath bezierPathWithRect:shadowView.bounds].CGPath;
        shadowView.layer.shadowPath = path;
        [self.contentView addSubview:shadowView];
                
        _thumbnailImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 8, 60, 60)];
        _thumbnailImageView.image = [UIImage imageNamed:@"facebook_pic_default"];
        _thumbnailImageView.contentMode = UIViewContentModeScaleAspectFill;
        [_thumbnailImageView setClipsToBounds:YES];
        [self.contentView addSubview:_thumbnailImageView];
        
        titleLable = [[UILabel alloc] initWithFrame:CGRectMake(90, 30, 210, 40)];
        titleLable.backgroundColor = UIColor.clearColor;
        titleLable.font = [Post fontForPostTitle];
        titleLable.shadowColor = [UIColor blackColor];
        titleLable.shadowOffset = CGSizeMake(0, .5);
        titleLable.numberOfLines = 0;
        titleLable.text = _title;
        titleLable.textColor = UIColor.whiteColor;
        [self.contentView addSubview:titleLable];
    }
    return self;
}

- (void)setTitle:(NSString *)title
{
    CGSize size = [title sizeWithFont:[Post fontForPostTitle] constrainedToSize:CGSizeMake(210, MAXFLOAT)
                        lineBreakMode:NSLineBreakByWordWrapping];
    
    titleLable.frame = CGRectMake(90, 30, 210, size.height);
    [titleLable setText:title];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

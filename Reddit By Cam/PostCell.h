//
//  PostCell.h
//  Reddit By Cam
//
//  Created by Cameron Clendenin on 7/20/13.
//  Copyright (c) 2013 Cameron Clendenin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostCell : UITableViewCell {

    UILabel *titleLable;
}

@property (nonatomic) UIView *customView;
@property (nonatomic) UIImageView *thumbnailImageView;
@property (nonatomic) NSString *author;
@property (nonatomic) NSString *title;

@end

Using CocoaPods to manage dependencies:

- First install cocoapods if you don't have it already: http://cocoapods.org
- Open the project using the  .xcworkspace  file instead of .xcodeproj
